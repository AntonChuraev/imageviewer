package com.example.imageloader.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.imageloader.CustomView
import com.example.imageloader.R
import com.example.imageloader.SquareImageView
import com.squareup.picasso.Picasso
import java.net.URL


/**
 * Created by Anton Churaev on 28.10.2020.
 */
class ListViewAdapter(private val context: Context,private val list: MutableList<String>): RecyclerView.Adapter<ListViewAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val image: CustomView = itemView.findViewById(R.id.imageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val listItem = list[position]

        (holder.image as CustomView).setData(listItem)

    }

}
