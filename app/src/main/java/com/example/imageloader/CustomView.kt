package com.example.imageloader


import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import com.squareup.picasso.Picasso


/**
 * Created by Anton Churaev on 29.10.2020.
 */
class CustomView:FrameLayout {

    private var backlight: ViewGroup? = null
    private var squareImageView:SquareImageView?=null

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0){
        build()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle){
        build()
    }

    constructor(context: Context):super(context){
        build()
    }


    fun build(){
        val v= LayoutInflater.from(this.getContext()).inflate(R.layout.view_backlight_item, this)
        backlight = v.findViewById(R.id.backlight)
        squareImageView = v.findViewById(R.id.square)
    }

    fun setData(url:String){
        Picasso.with(context).load(url).into(squareImageView)
    }

}