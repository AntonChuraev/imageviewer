package com.example.imageloader.mvp

import com.example.imageloader.mvp.intefaces.PresenterInterface
import com.example.imageloader.mvp.intefaces.ViewInterface

/**
 * Created by Anton Churaev on 28.10.2020.
 */
class Presenter(val view: ViewInterface): PresenterInterface {
    val model = Model()

    override fun startLoading() {
        view.showResult(model.loadInformation())
    }
}