package com.example.imageloader.mvp.intefaces

import io.reactivex.Observable

/**
 * Created by Anton Churaev on 29.10.2020.
 */
interface ModelInterface{
    fun loadInformation(): Observable<MutableList<String>>?
}