package com.example.imageloader.mvp.intefaces

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface RetrofitServicesInterface {
    @GET("?a=getRandom")
    fun getList(@Query("count")count:Int): Observable<MutableList<String>>
}

