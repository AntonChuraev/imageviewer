package com.example.imageloader.mvp.intefaces

/**
 * Created by Anton Churaev on 29.10.2020.
 */
interface PresenterInterface{
    fun startLoading()
}