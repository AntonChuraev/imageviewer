package com.example.imageloader.mvp

import android.util.Log
import com.example.imageloader.mvp.intefaces.ModelInterface
import com.example.imageloader.mvp.intefaces.RetrofitServicesInterface

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception

/**
 * Created by Anton Churaev on 28.10.2020.
 */
class Model: ModelInterface {



    val retrofit = Retrofit.Builder()
        .baseUrl("http://bnet.i-partner.ru/projects/calc/networkimages/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val lastFmService = retrofit.create(RetrofitServicesInterface::class.java)



    override fun loadInformation(): Observable<MutableList<String>>? {
        val response=lastFmService.getList(100).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

        return response
    }


    fun handleResult(list:MutableList<String>){
        Log.i("TAG3" , list.toString())
    }

    fun handleError(e:Throwable){
        throw Exception(e)
    }


}





