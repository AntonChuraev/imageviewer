package com.example.imageloader

import android.content.res.Configuration
import android.os.Bundle
import android.os.Parcelable
import android.os.PersistableBundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.imageloader.mvp.Presenter
import com.example.imageloader.mvp.intefaces.ViewInterface
import com.example.imageloader.adapters.ListViewAdapter
import io.reactivex.Observable
import java.lang.Exception


class MainActivity : AppCompatActivity(), ViewInterface {

    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapter: ListViewAdapter

    val itemsInRowVerticalOrientation=2
    val itemsInRowHorizontalOrientation=3

    lateinit var recyclerView:RecyclerView

    private val KEY_RECYCLER_STATE = "recycler_state"

    var mListState: Parcelable?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i("TAG" , "onCreate")

        recyclerView=findViewById(R.id.recyclerList)

        layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        layoutManager = GridLayoutManager(this, itemsInRowVerticalOrientation)
        recyclerView.layoutManager = layoutManager

        val presenter = Presenter(this)
        presenter.startLoading()
    }


    override fun showResult(response: Observable<MutableList<String>>?) {
        if (response==null){
            throw Exception("Null response")
        }

        Log.i("TAG" , "showResult")
        response.doOnNext( this::showAdapter).doOnError(this::showException).subscribe()


    }

    fun showAdapter(list:MutableList<String>){
        Log.i("TAG" , list.toString())

        for (i in 0..list.size-1){
            val item=list.get(i)
            if (!itemUrlRight(item)){
                val changedItem=createChangedItem(item)
                list.set(i,changedItem)
            }
        }

        adapter = ListViewAdapter(this , list)
        recyclerView.adapter = adapter
    }

    fun showException(e:Throwable){
        e.printStackTrace()
    }

    private fun createChangedItem(item: String): String {
        return "http://bnet.i-partner.ru/projects/calc/networkimages${item.substring(1,item.length)}"
    }

    private fun itemUrlRight(item: String): Boolean {
        return !item.startsWith(".")
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.i("TAG" , "landscape")

            layoutManager = GridLayoutManager(this, itemsInRowHorizontalOrientation)
            recyclerView.layoutManager=layoutManager
            recyclerView.adapter!!.notifyDataSetChanged()

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.i("TAG" , "portrait")

            layoutManager = GridLayoutManager(this, itemsInRowVerticalOrientation)
            recyclerView.layoutManager=layoutManager
            recyclerView.adapter!!.notifyDataSetChanged()
        }

    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

        mListState = layoutManager.onSaveInstanceState()!!
        outState.putParcelable(KEY_RECYCLER_STATE, mListState);
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        if(savedInstanceState != null)
            mListState = savedInstanceState.getParcelable(KEY_RECYCLER_STATE)!!
    }

    override fun onResume() {
        super.onResume()


        if (mListState != null) {
            layoutManager.onRestoreInstanceState(mListState);
        }
    }
}
